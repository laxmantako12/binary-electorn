jQuery(document).ready(function ($) {
    function custommatchHeight() {
        var options = ({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
        $('.why-choose-us__block .title h4').matchHeight(options);
        $('.amenities__block').matchHeight(options);
        $('.services__block').matchHeight(options);

    }

    custommatchHeight()


    // svg image to code
    jQuery('img.svg-icon').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });

    $(function () {
        let slider = $('.testimonial__slider');
        slider.slick({
            infinite: true,
            speed: 2000,
            slidesToShow: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            fade: false,
            arrows: false,
            // appendArrows: $('.testimonial__arrows'),
            // prevArrow: '<div class="testimonial__arrow testimonial__arrow_dir_left"></div>',
            // nextArrow: '<div class="testimonial__arrow testimonial__arrow_dir_right"></div>',
            dots: true,
            appendDots: $('.testimonial__dots'),
            customPaging: function (slider, i) {
                var thumb = $(slider.$slides[i]).data();

                return '0' + (i + 1);
            },
            dotsClass: 'testimonial__dots-list',
        });
    });

    function slideGo(dir) {
        let slider = $('.testimonial__slider');
        if (dir === "+") {
            slider.slick('slickNext');
        } else if (dir === "-") {
            slider.slick('slickPrev');
        }
    }


    $('[data-toggle="tooltip"]').tooltip()


    //   nav show and hide
    $('body').on('click', '.navbar-toggler', function () {
        $(this).toggleClass('on');
        $('.navbar-collapse').toggleClass('slide');
    });

    $('.navbar-nav>li>a').on('click', function(){
        $('.navbar-collapse').toggleClass('slide');
    });


    function fixHeaderSpacing() {
        var headerHeight = $('.navbar').outerHeight();
        $('body').css({
            // 'padding-top': headerHeight,
        });
    }

    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
    
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 70
        }, 500);
    });

    fixHeaderSpacing();
    function particleAnimateOne() {
        $(".particle-class").each(function () {
            particlesJS($(this).attr('id'), {
                particles: {
                    color: '#fff',
                    shape: 'circle', // "circle", "edge" or "triangle"
                    opacity: 0.5,
                    size: 6,
                    size_random: true,
                    nb: 250,
                    line_linked: {
                        enable_auto: true,
                        distance: 5,
                        color: '#fff',
                        opacity: 1,
                        width: 2,
                        condensed_mode: {
                            enable: false,
                            rotateX: 600,
                            rotateY: 600
                        }
                    },
                    anim: {
                        enable: true,
                        speed: 1
                    }
                },
                interactivity: {
                    enable: true,
                    mouse: {
                        distance: 300
                    },
                    detect_on: 'canvas', // "canvas" or "window"
                    mode: 'grab',
                    line_linked: {
                        opacity: .5
                    },
                    events: {
                        onclick: {
                            enable: true,
                            mode: 'push', // "push" or "remove" (particles)
                            nb: 4
                        }
                    }
                },
                /* Retina Display Support */
                retina_detect: true
            });
        });

    }



    particleAnimateOne();

    // dynamic year

    const paragraph = `&copy; Copyright  ${new Date().getFullYear()}  All Rights Reserved.`;

    document.getElementById('copyright').innerHTML = paragraph;


});

jQuery(window).on("load", function () {
    var $ = jQuery

    $('body').addClass('loaded');
    $('.loader').fadeOut('2000');
});
